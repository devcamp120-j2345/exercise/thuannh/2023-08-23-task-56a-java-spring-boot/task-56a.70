package com.devcamp.stringrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringrestapiApplication.class, args);
	}

}
